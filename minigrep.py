#!/usr/bin/env python3

import argparse
import os
import sys
import re

# Output sections of coloured strings - can this be replaced by string.replace()?
def colourise(line, pattern_len, indexes):
    uncoloured = line
    offset = 0
    for i in indexes:
        index = i + offset
        # Insert colour codes arround pattern
        uncoloured = uncoloured[:index] + '\033[31;1m' + uncoloured[index:index+pattern_len] + '\033[m' + uncoloured[index+pattern_len:]
        offset += len('\033[31;1m' + '\033[m')
    return(uncoloured)


def find_indexes(pattern, line):
    # Don't know if re is allowed? Tried without before...
    return [r.start() for r in re.finditer(pattern, line)]


def match_file(infile, args, prefix=''):
    found = False
    count = 0

    for line in infile:
        format_line = line.rstrip()

        # Find where the matches are on a line, ignore case if needs be
        indexes = find_indexes(args.pattern.lower(), format_line.lower()) if args.ignore_case else find_indexes(args.pattern, format_line)

        # If we have a match and we care
        if len(indexes) > 0 and not args.invert_match:
            # Colour it if needs be or just flag as found
            format_line = colourise(format_line, len(args.pattern), indexes) if args.colour else format_line
            found = True

        # Prepend line count
        if args.line_number: format_line = '\033[32m' + str(count) + '\033[34m' +  ":" + '\033[m' + format_line

        # Match XOR no match needed -> print
        if bool(len(indexes) > 0) != args.invert_match: print(format_line)

        count+=1

    return found


if __name__ == '__main__':

    # Argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--ignore-case', action='store_true', help='Case insensitive matching')
    parser.add_argument('-v', '--invert-match', action='store_true', help='Reverse match')
    parser.add_argument('-n', '--line-number', action='store_true', help='Show line number')
    parser.add_argument('--colour', action='store_true', help='Coloured output')
    parser.add_argument('pattern', help='Pattern to match')
    parser.add_argument('files', nargs='*', help='input files', default=['-'])

    args = parser.parse_args()

    # Match files
    found = False
    prefix = ''
    for filename in args.files:

        if filename == '-':
            filename = '/dev/stdin'

        if len(args.files) > 1:
            prefix = filename

        try:
            with open(filename) as file:
                if match_file(file, args, prefix=prefix):
                    found = True
        except Exception as e:
            print("{}: {}".format(filename, e))
            sys.exit(2)

    if found:
        sys.exit(0)  # 0 if we have a match

    sys.exit(1)      # 1 no match
